package com.petric.juc.aqs.util;

import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentLinkedQueue;

public class TestUnsafe {
    private transient volatile Node head;


    private transient volatile Node tail;

    /**
     * Creates a {@code ConcurrentLinkedQueue} that is initially empty.
     */
    public TestUnsafe() {
        head = tail = new Node(null);
    }
    public static void main(String[] args) {
        TestUnsafe testUnsafe = new TestUnsafe();
        testUnsafe.offer("diyige");
    }

    public void offer(String item){
        final Node newNode = new Node(item);
        Node t = tail, p = t;
        Node q = p.next;
        if (q == null) {
            if (p.casNext(null, newNode)) {
                System.out.println("-------------");
            }
        }
    }


    private static class Node{

        volatile String item;

        volatile Node next;

        /**
         * 使用Unsafe CAS方法
         * @param cmp 目标值与cmp比较，如果相等就更新返回true；如果不相等就不更新返回false；
         * @param val 需要更新的值；
         * @return
         */
        boolean casNext(Node cmp, Node val) {
            /**
             * compareAndSwapObject(Object var1, long var2, Object var3, Object var4)
             * var1 操作的对象
             * var2 操作的对象属性
             * var3 var2与var3比较，相等才更新
             * var4 更新值
             */
            return UNSAFE.compareAndSwapObject(this, nextOffset, cmp, val);
        }

        Node(String item) {
            UNSAFE.putObject(this, itemOffset, item);
        }

        private static final sun.misc.Unsafe UNSAFE;
        private static final long nextOffset;
        private static final long itemOffset;
        static {
            try {
                UNSAFE = getUnsafe();
                Class<?> k = Node.class;
                nextOffset = UNSAFE.objectFieldOffset
                        (k.getDeclaredField("next"));

                itemOffset = UNSAFE.objectFieldOffset
                        (k.getDeclaredField("item"));
            } catch (Exception e) {
                throw new Error(e);
            }
        }

        /**
         * 获取Unsafe的方法
         * 获取了以后就可以愉快的使用CAS啦
         * @return
         */
        public static Unsafe getUnsafe() {
            try {
                Field f = Unsafe.class.getDeclaredField("theUnsafe");
                f.setAccessible(true);
                return (Unsafe)f.get(null);
            } catch (Exception e) {
                return null;
            }
        }
    }
}
