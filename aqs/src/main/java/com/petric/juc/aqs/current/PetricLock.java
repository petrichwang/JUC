package com.petric.juc.aqs.current;

import com.petric.juc.aqs.util.UnsafeInstance;
import sun.misc.Unsafe;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.LockSupport;

/**
 * AQS核心三把斧头：自旋、LockSupport、CAS
 * AQS：可中断，锁可重入，锁竞争可以公平也可以非公平
 * 排他锁，共享锁
 * <p>
 * PetricLock这个是个公平锁
 * <p>
 */
public class PetricLock {

    /**
     * 竞争这个状态，
     * 多线程修改state状态，任意时刻只能有一个线程成功，不能加锁synchronized，采用无锁算法cas
     */
    private volatile int state = 0;

    /**
     * 记录当前哪个线程获取了该锁
     */
    private Thread lockHolder;

    /**
     * 同步队列
     */
    private final ConcurrentLinkedQueue<Thread> queue = new ConcurrentLinkedQueue<>();

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Thread getLockHolder() {
        return lockHolder;
    }

    public void setLockHolder(Thread lockHolder) {
        this.lockHolder = lockHolder;
    }


    /**
     * 加锁
     */
    public void lock() {
        // 一起去抢state = 0；只有为0时表示当前锁时自由的可以抢；
        // T1加锁成功
        if (acquire()) {
            return;
        }
        // 加锁失败
        Thread current = Thread.currentThread();
        queue.add(current);

        // spin 自旋，直到加锁成功
        for (; ; ) {
            if ((queue.peek() == current) && acquire()) { // 拿队列的第一个线程，唤醒它再次尝试加锁
                queue.poll(); // 线程被唤醒，拿到锁，从队列移除
                return;
            }
            // T2, T3执行到这里被停住，防止浪费CPU资源
            LockSupport.park();
        }
    }

    /**
     * 解锁
     */
    public void unlock() {
        Thread current = Thread.currentThread();
        if (current != lockHolder) { // 当前拿到锁的线程是否lockHolder
            throw new RuntimeException("couldn't release lock");
        }
        int c = getState();
        if (compareAndSwapState(1, 0)) { // T1释放锁
            setLockHolder(null);
            Thread thread = queue.peek(); // 唤醒队列头的线程
            LockSupport.unpark(thread); // 唤醒线程
        }
    }

    private boolean acquire() {
        Thread current = Thread.currentThread();
        int state = getState();
        if (state == 0) { // 表示当前线程可以加锁
            ConcurrentLinkedQueue<Thread> q = this.queue;
            if ((q.size() == 0 || current == queue.peek()) && compareAndSwapState(0, 1)) {
                setLockHolder(current);
                return true;
            }
        }
        return false;
    }

    /**
     * 定义一个cas方法
     *
     * @param except
     * @param update
     * @return
     */
    public boolean compareAndSwapState(int except, int update) {
        return unsafe.compareAndSwapInt(this, stateOffset, except, update);
    }

    /**
     * getUnsafe()可以绕过JVM虚拟机直接操作内存里面要指定的那个变量；
     * 不安全类，所以不允许我们直接使用这个类，
     * 要使用这个Unsafe只有通过引导类去加载，否则要使用则要用反射机制
     */
    // 1 获取失败，只能通过反射
//    private static final Unsafe unsafe = Unsafe.getUnsafe();
    // 2 反射获取
    private static final Unsafe unsafe = UnsafeInstance.reflectGetUnsafe();

    /**
     * state变量的内存地址（偏移量）
     * 偏移量：当前对象起始位置+变量的偏移量=变量的值的位置，找到这个位置就可以修改这个对象的这个变量的值
     */
    private static final long stateOffset;

    static {
        try {
            stateOffset = unsafe.objectFieldOffset(PetricLock.class.getDeclaredField("state"));
        } catch (Exception e) {
            throw new Error();
        }
    }
}
