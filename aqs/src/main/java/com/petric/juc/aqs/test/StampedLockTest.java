package com.petric.juc.aqs.test;

import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.StampedLock;

public class StampedLockTest {

    static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    static final ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
    static final ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();


    public static void main(String[] args) throws InterruptedException {
//        reentrantReadWriteLockTest();

//        test1();
//        distanceFromOrigin();

        conditionReadWrite();
    }

    private static int balance=200;
    static final StampedLock stampedLock = new StampedLock();

    /**
     * 简单应用
     *
     * @throws InterruptedException
     */
    private static void test1() throws InterruptedException {

        // readLock可重入
        Thread t1 = new Thread(() -> {
            long stamp = stampedLock.readLock();
            long stamp2 = stampedLock.readLock();
            try {
                System.out.println("t1 start");
                int c = balance;
                System.out.println("t1 end");

            } finally {
                stampedLock.unlockRead(stamp);
                stampedLock.unlockRead(stamp2);

            }
        });

        // writeLock不可重入
        Thread t2 = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            long stamp = stampedLock.writeLock();
//            long stamp2 = stampedLock.writeLock();

            try {
                System.out.println("t2 start");
                balance += 1;
                System.out.println("t2 end");
            } finally {
                stampedLock.unlockWrite(stamp);
//                stampedLock.unlockWrite(stamp2);
            }
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();
        System.out.println("main is over! balance=" + balance);
    }

    /**
     * 乐观锁1
     */
    public static void distanceFromOrigin() throws InterruptedException {

        Thread read = new Thread(() -> {
            long stamp = stampedLock.tryOptimisticRead();
            System.out.println("t1 read first balance start");
            int c = balance;
            System.out.println("t1 read first balance end, c=" + c);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 如果有写锁被占用，可能造成数据不一致，所以要切换到普通读锁模式
            if (!stampedLock.validate(stamp)) {
                // 重新读取
                stamp = stampedLock.readLock();
                try {
                    System.out.println("t1 read second balance start");
                    c = balance;
                    System.out.println("t1 read second balance end, c=" + c);
                } finally {
                    stampedLock.unlockRead(stamp);
                }
            }
        });

        Thread write = new Thread(() -> {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            long stamp = stampedLock.writeLock();
            try {
                System.out.println("write start");
                balance += 1;
                System.out.println("write end");
            } finally {
                stampedLock.unlockWrite(stamp);
            }
        });

        read.start();
        write.start();

        read.join();
        write.join();

        System.out.println("main is over! balance=" + balance);
    }

    /**
     * 升级为写锁
     * balance >100才更新 see moveIfAtOrigin
     */
    public static void conditionReadWrite() throws InterruptedException {

        Thread t1 = new Thread(() -> {
            // 暂先判断balance的值是否符合更新条件
            System.out.println("t1 read start, balance=" + balance);
            long stamp = stampedLock.readLock();
            System.out.println("t1 read end, balance=" + balance);

            try {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                while (balance > 100) {
                    long writeStamp = stampedLock.tryConvertToWriteLock(stamp);
                    if (writeStamp != 0) { // 成功转换写锁
                        stamp = writeStamp;
                        System.out.println("t1 write balance start, balance=" + balance);
                        balance += 1000;
                        System.out.println("t1 write balance end, balance=" + balance);
                        break;
                    } else {
                        // 没有转换成功写锁，这里需要首先释放锁，然后再拿到写锁
                        stampedLock.unlockRead(stamp);
                        //获取写锁
                        stamp = stampedLock.writeLock();
                    }
                }
            } finally {
                stampedLock.unlock(stamp);

            }
        });

        Thread t2 = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            long stamp = stampedLock.writeLock();
            try {
                System.out.println("t2 start");
                balance -= 1000;
                System.out.println("t2 end, balance=" + balance);
            } finally {
                stampedLock.unlockWrite(stamp);
            }
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        System.out.println("main is over! balance=" + balance);

    }


    /**
     * 可重入
     */
    private static void reentrantReadWriteLockTest() {
        Thread readThread = new Thread(() -> {

            readLock.lock();
            readLock.lock();
            try {
                System.out.println("readThread==>start");
                System.out.println("readThread==>end");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                readLock.unlock();
//                readLock.unlock();  // 注释的话，永远占用读锁；写锁同理

            }
        });

        Thread writeThread = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            writeLock.lock();
            writeLock.lock();
            try {
                System.out.println("writeThread==>start");

                System.out.println("writeThread==>end");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                writeLock.unlock();
                writeLock.unlock();

            }
        });

        readThread.start();
        writeThread.start();

    }


}
