package com.petric.juc.aqs.test;

import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReentrantReadWriteLockTest {

    static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    static final ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
    static final ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();

    public static void main(String[] args) {
//        test1();

//        test2();

        test3();
    }

    /**
     * 第一种，读的时候，写不能获取到锁
     */
    private static void test1() {
        Thread readThread = new Thread(() -> {
            readLock.lock();
            try {
                System.out.println("readThread==>start");
                Thread.sleep(10000);
                System.out.println("readThread==>end");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                readLock.unlock();

            }
        });

        Thread writeThread = new Thread(() -> {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            writeLock.lock();

            try {
                System.out.println("writeThread==>start");

                System.out.println("writeThread==>end");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                writeLock.unlock();
            }
        });

        readThread.start();
        writeThread.start();

    }

    /**
     * 第二种，写的时候，读不能获取到锁
     */
    private static void test2() {
        Thread readThread = new Thread(() -> {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            readLock.lock();
            try {
                System.out.println("readThread==>start");

                System.out.println("readThread==>end");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                readLock.unlock();

            }
        });

        Thread writeThread = new Thread(() -> {


            writeLock.lock();

            try {
                System.out.println("writeThread==>start");
                Thread.sleep(100000);
                System.out.println("writeThread==>end");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                writeLock.unlock();
            }
        });
        readThread.start();
        writeThread.start();
    }

    /**
     * 第三种，读的时候，读能获取到锁
     */
    private static void test3() {
        Thread readThread1 = new Thread(() -> {

            readLock.lock();
            try {
                System.out.println("readThread1==>start");
                Thread.sleep(10000);
                System.out.println("readThread1==>end");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                readLock.unlock();

            }
        });

        Thread readThread2 = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            readLock.lock();
            try {
                System.out.println("readThread2==>start");
                System.out.println("readThread2==>end");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                readLock.unlock();
            }
        });

        readThread1.start();
        readThread2.start();
    }


}
