package com.petric.juc.aqs.controller;

import com.petric.juc.aqs.service.TradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PublicController {
    @Autowired
    private TradeService tradeService;


    @GetMapping(value = "/order")
    public String order() {
        return tradeService.decStockNoLock();
//        return tradeService.decStockLock();
    }
}
