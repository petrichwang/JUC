package com.petric.juc.aqs.service;

import com.petric.juc.aqs.current.PetricLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 注意：只考虑单机模式
 */
//@Slf4j
@Service
public class TradeService {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 减库存
     * 线程不安全；导致超卖
     *
     * @return
     */
    public String decStockNoLock() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Integer stock;
        List<Map<String, Object>> result =
                jdbcTemplate.queryForList("select stock from aqs where id=1");
        if (result == null || (stock = (Integer) result.get(0).get("stock")) <= 0) {
//            log.warn("下单失败，已经没有库存了"); // throw
            return "下单失败，已经没有库存了";
        }

        stock--; // volatile is not useful, ps: stock is from database
        jdbcTemplate.update("update aqs set stock=? where id=1", stock);
//        log.info("下单成功，当前剩余产品数量：--->{}", stock);
        return "下单成功，当前剩余产品数量：--->" + stock;
    }

    /**
     * 并发同步器
     * AbstractQueuedSynchronizer(AQS)
     * <p>
     * ReentrantLock 可重入锁
     * lock() 加锁
     * unlock() 解锁
     */
    private ReentrantLock lock = new ReentrantLock();
//    private NonReentrantLock lock = new NonReentrantLock();
    /**
     * 减库存
     * 线程安全
     *
     * @return
     */
    public String decStockLock() {
        lock.lock(); // (1)
        Integer stock = null;

        try {
            Thread.sleep(1000);

            List<Map<String, Object>> result =
                    jdbcTemplate.queryForList("select stock from aqs where id=1");
            if (result == null || (stock = (Integer) result.get(0).get("stock")) <= 0) {
//                log.warn("下单失败，已经没有库存了"); // throw
                return "下单失败，已经没有库存了";
            }

            stock--;
            jdbcTemplate.update("update aqs set stock=? where id=1", stock);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock(); // (2)
        }

//        log.info("下单成功，当前剩余产品数量：--->{}", stock);
        return "下单成功，当前剩余产品数量：--->" + stock;
    }

    private PetricLock petricLock = new PetricLock();

    /**
     * 减库存
     * 线程安全
     *
     * @return
     */
    public String decStockPetricLock() {
        petricLock.lock(); // (1)
        Integer stock = null;

        try {
            Thread.sleep(1000);

            List<Map<String, Object>> result =
                    jdbcTemplate.queryForList("select stock from aqs where id=1");
            if (result == null || (stock = (Integer) result.get(0).get("stock")) <= 0) {
//                log.warn("下单失败，已经没有库存了"); // throw
                return "下单失败，已经没有库存了";
            }

            stock--;
            jdbcTemplate.update("update aqs set stock=? where id=1", stock);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            petricLock.unlock(); // (2)
        }

//        log.info("下单成功，当前剩余产品数量：--->{}", stock);
        return "下单成功，当前剩余产品数量：--->" + stock;
    }
}
