package com.petric.juc.aqs.test;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcurrentLinkedQueueTest {
    static LinkedBlockingQueue<String> lbq = new LinkedBlockingQueue<>(3);



    public static void main(String[] args) throws InterruptedException {



        ConcurrentLinkedQueue<String> clq = new ConcurrentLinkedQueue<>();
        clq.offer("diyige");
        clq.offer("dierge");
        clq.offer("disange");

        System.out.println(clq.size());
        System.out.println(clq.poll());
        System.out.println(clq.poll());
        System.out.println(clq.size());
    }
}
