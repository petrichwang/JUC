package com.petric.juc.aqs.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class LinkedBlockingQueueTest {
    static class MyRunnable implements Runnable {
        private String a;

        public MyRunnable(String a) {
            this.a = a;
        }

        @Override
        public void run() {
            try {
                abq.put(a);
                lbq.put(a);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static LinkedBlockingQueue<String> lbq = new LinkedBlockingQueue<>(3);
    static ArrayBlockingQueue<String> abq = new ArrayBlockingQueue<>(3, true);

    public static void main(String[] args) throws InterruptedException {
        List<Thread> l
                = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            l.add(new Thread(new MyRunnable(i + "")));
        }
        l.forEach(Thread::start);

       /* for (int i = 100; i < 200; i++) {
            new Thread(new MyRunnable(i + "")).start();
        }*/


        Thread t3 = new Thread(() -> {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (; ; ) {
                try {
                    System.out.println(abq.take());
//                    System.out.println(lbq.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });


        t3.start();

        t3.join();
        System.out.println("main is over!");
    }
}
