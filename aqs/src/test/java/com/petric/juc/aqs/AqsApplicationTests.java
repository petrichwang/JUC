package com.petric.juc.aqs;

import com.petric.juc.aqs.service.TradeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AqsApplicationTests {
    @Autowired
    private TradeService tradeService;

    /**
     * https://www.cnblogs.com/lucare/p/9313191.html
     */
    private static final Integer THREAD_COUNT = 10;
    private static CountDownLatch latch = new CountDownLatch(THREAD_COUNT);

    @Test
    public void decStockNoLock() throws InterruptedException {
        long start = System.currentTimeMillis();

        List<Thread> list = new ArrayList<>();
        for (int i = 0; i < THREAD_COUNT; i++) {
            list.add(new Thread(() -> {
                System.err.println(tradeService.decStockNoLock());
                latch.countDown();
            }));

        }
        list.parallelStream().forEach(Thread::start);

        latch.await();

        long end = System.currentTimeMillis();
        System.out.println("main is over, " + (end - start) + "ms");
    }

    @Test
    public void decStockLock() throws InterruptedException {
        long start = System.currentTimeMillis();

        List<Thread> list = new ArrayList<>();
        for (int i = 0; i < THREAD_COUNT; i++) {
            list.add(new Thread(() -> {
                System.err.println(tradeService.decStockLock());
                latch.countDown();
            }));

        }
        list.parallelStream().forEach(Thread::start);

        latch.await();

        long end = System.currentTimeMillis();
        System.out.println("main is over, " + (end - start) + "ms");

    }

    @Test
    public void decStockPetricLock() throws InterruptedException {
        long start = System.currentTimeMillis();

        List<Thread> list = new ArrayList<>();
        for (int i = 0; i < THREAD_COUNT; i++) {
            list.add(new Thread(() -> {
                System.err.println(tradeService.decStockPetricLock());
                latch.countDown();
            }));

        }
        list.parallelStream().forEach(Thread::start);

        latch.await();

        long end = System.currentTimeMillis();
        System.out.println("main is over, " + (end - start) + "ms");

    }

    public static void main(String[] args) throws InterruptedException {

//        lockAndLockInterrupt();

        /*ReentrantLock lock = new ReentrantLock();
        new Thread(() -> {
            try {
                lock.lock();
                Thread.sleep(2000000000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        },"TT").start();

        Thread t1 = new Thread(() -> {
            try {
                Thread.sleep(100);
                lock.lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        },"t1");

        Thread t2 = new Thread(() -> {
            try {
                Thread.sleep(100);
                lock.lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        },"t2");

        t1.start();
        t2.start();*/


       /* ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
        ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
        ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();

        try {
            writeLock.lock();
            System.out.println("--------------ok");
            readLock.lock();

        } finally {
            writeLock.unlock();
            readLock.unlock();

        }*/


    }

    private static void lockAndLockInterrupt() throws InterruptedException {
        ReentrantLock lock = new ReentrantLock();
        Thread t1 = new Thread(() -> {
            try {
                lock.lock();
                Thread.sleep(5000);
                System.out.println("run t1");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }, "t1");

        Thread t2 = new Thread(() -> {
            try {
                Thread.sleep(1000);
                lock.lock(); // @see AQS -> acquire(int arg) -> selfInterrupt();不抛异常，将继续执行
//                lock.lockInterruptibly();
                System.out.println("run t2");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }, "t2");

        t1.start();
        t2.start();
        Thread.sleep(2000);

        t2.interrupt();
        t1.join();
        t2.join();
        System.out.println("main is over");
    }


}
