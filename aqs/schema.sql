SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for aqs
-- ----------------------------
DROP TABLE IF EXISTS `aqs`;
CREATE TABLE `aqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of aqs
-- ----------------------------
INSERT INTO `aqs` VALUES ('1', '5');
